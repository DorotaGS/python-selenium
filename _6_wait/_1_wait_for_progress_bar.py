import pytest
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample6.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_wait_for_progress_bar_to_finish(driver: Remote):
    # TODO Find and click Load button
    # TODO Wait for progress bar to finish
    assert "width: 100%;" == driver.find_element_by_css_selector(".md-progress-bar .md-progress-bar-fill").get_attribute("style")
