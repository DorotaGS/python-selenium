class CustomList(object):
    def __init__(self, items=[]):
        self.items = items

    def clear(self):
        self.items.clear()
        return self.items

    def len(self):
        return len(self.items)
