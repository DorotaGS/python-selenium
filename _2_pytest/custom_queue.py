class CustomQueue(object):
    def __init__(self, items=[]):
        self.items = items

    def is_empty(self):
        return self.items == []

    def add(self, item):
        self.items.insert(0, item)

    def remove(self):
        return self.items.pop()

    def size(self):
        return len(self.items)
