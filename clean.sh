#!/bin/sh

# rm -rf .idea
# rm -rf venv
find . -name geckodriver.log -type f -delete
find screenshots ! -name '.gitkeep' -type f -delete
find . -name '__pycache__' -exec rm -rf "{}" \;
find . -name '.pytest_cache' -exec rm -rf "{}" \;
find . -name '.DS_Store' -exec rm -rf "{}" \;